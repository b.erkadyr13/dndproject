// Fill out your copyright notice in the Description page of Project Settings.


#include "CharactersAttributeSet.h"
#include "GameplayEffectExtension.h"

UCharactersAttributeSet::UCharactersAttributeSet() {
}

void UCharactersAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	AActor* OwningActor = Data.Target.AbilityActorInfo->OwnerActor.Get();

	if (Data.EvaluatedData.Attribute == GetHealthAttribute()) {
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetHealthMax()));
		OnHealthChanged.Broadcast(OwningActor, GetHealth());
	}
	
	if (Data.EvaluatedData.Attribute == GetActionPointsAttribute()) {
		SetActionPoints(FMath::Clamp(GetActionPoints(), 0.0f, GetStarterActionPoints()));
	}
	if (Data.EvaluatedData.Attribute == GetArmorAttribute()) {
		SetArmor(FMath::Clamp(GetArmor(), 0.0f, 100));
	}
	if (Data.EvaluatedData.Attribute == GetAttackAttribute()) {
		SetAttack(FMath::Clamp(GetAttack(), 0.0f, 100));
	}
	if (Data.EvaluatedData.Attribute == GetStrengthAttribute()) {
		SetStrength(FMath::Clamp(GetStrength(), 0.0f, 100));
	}
	if (Data.EvaluatedData.Attribute == GetAgilityAttribute()) {
		SetAgility(FMath::Clamp(GetAgility(), 0.0f, 100));
	}
	if (Data.EvaluatedData.Attribute == GetIntelligenceAttribute()) {
		SetIntelligence(FMath::Clamp(GetIntelligence(), 0.0f, 100));
	}
	if (Data.EvaluatedData.Attribute == GetVigorAttribute()) {
		SetVigor(FMath::Clamp(GetVigor(), 0.0f, 100));
	}
	if (Data.EvaluatedData.Attribute == GetKnowledgeAttribute()) {
		SetKnowledge(FMath::Clamp(GetKnowledge(), 0.0f, 100));
	}
	if (Data.EvaluatedData.Attribute == GetSpeedAttribute()) {
		SetSpeed(FMath::Clamp(GetSpeed(), 0.0f, 100));
	}
}