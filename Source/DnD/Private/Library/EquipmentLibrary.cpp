

#include "Library/EquipmentLibrary.h"

#include "Components/EquipmentComponent.h"
#include "Interface/EquipmentSystemInterface.h"

UEquipmentComponent* UEquipmentLibrary::GetEquipmentSystemComponent(AActor* Actor)
{
	return GetEquipmentSystemComponentFromActor(Actor);
}

UEquipmentComponent* UEquipmentLibrary::GetEquipmentSystemComponentFromActor(const AActor* Actor, bool LookForComponent)
{
	if (Actor)
	{
		if (const auto ISI = Cast<IEquipmentSystemInterface>(Actor))
		{
			return ISI->GetEquipmentSystemComponent();
		}
		else if (LookForComponent)
		{
			// Fall back to a component search to better support BP-only actors
			return Actor->FindComponentByClass<UEquipmentComponent>();
		}
	}

	return nullptr;
}