

#include "Library/TimelineLibrary.h"

#include "Components/TimelineComponent.h"

#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

bool UTimelineLibrary::BindFuncTimeline(FTimeline& Timeline, UCurveFloat* Curve, UObject* FuncInClass, const FName& InFunctionName, bool bAutoPlay, float Rate)
{
	if (Curve)
	{
		// ������� �����������.
		FOnTimelineFloat TimelineFunc;

		// ����������� �������.
		TimelineFunc.BindUFunction(FuncInClass, InFunctionName);

		// ��������� ������� � ���������.
		Timeline.AddInterpFloat(Curve, TimelineFunc);

		// ������������� �������� ���������.
		Timeline.SetPlayRate(Rate);

		// ���� ����� ������ �������������, ��������� ������.
		if (bAutoPlay) { Timeline.Play(); }

		return true;
	}
	return false;
}

bool UTimelineLibrary::BindFuncTimeline(FTimeline& Timeline, UObject* FuncInClass, const FName& InFunctionName)
{
	if ((FuncInClass != nullptr) && (FuncInClass->FindFunction(InFunctionName) != nullptr))
	{
		FOnTimelineEvent FinishTimeline;

		// ����������� �������, ������� ���������� �� ���������� ������������ ���������.
		FinishTimeline.BindUFunction(FuncInClass, InFunctionName);

		// ������������� �������.
		Timeline.SetTimelineFinishedFunc(FinishTimeline);

		return true;
	}
	return false;
}