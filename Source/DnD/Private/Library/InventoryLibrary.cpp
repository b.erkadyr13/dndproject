
#include "Library/InventoryLibrary.h"

#include "Components/InventoryComponent.h"
#include "Interface/InventorySystemInterface.h"

UInventoryComponent* UInventoryLibrary::GetInventorySystemComponent(AActor* Actor)
{
	return GetInventorySystemComponentFromActor(Actor);
}

UInventoryComponent* UInventoryLibrary::GetInventorySystemComponentFromActor(const AActor* Actor, bool LookForComponent)
{
	if (Actor)
	{
		if (const auto ISI = Cast<IInventorySystemInterface>(Actor))
		{
			return ISI->GetInventorySystemComponent();
		}
		else if (LookForComponent)
		{
			// Fall back to a component search to better support BP-only actors
			return Actor->FindComponentByClass<UInventoryComponent>();
		}
	}

	return nullptr;
}

int32 UInventoryLibrary::RemoveItemByItemFromInventory(AActor* InActor, UItemData* ItemData, int32 Amount)
{
	if ((ItemData != nullptr) && (Amount > 0))
	{
		if (auto ISC = GetInventorySystemComponent(InActor))
		{
			int32 RemovedItem = 0;

			for (int32 i = 0; i < Amount; i++)
			{
				int32 ArrId;
				FInventorySlot SlotLocal;
				if (ISC->FindSlotByItem(ItemData, ArrId, SlotLocal, false))
				{
					RemovedItem++;
					ISC->RemoveItemAt(ArrId);
				}
			}
			return RemovedItem;
		}
	}
	return 0;
}

bool UInventoryLibrary::RemoveItemByUniqIdFromInventory(AActor* InActor, int32 UniqueID)
{
	if (auto ISC = GetInventorySystemComponent(InActor))
	{
		int32 ArrID;
		FInventorySlot Slot;
		if (ISC->FindSlotByUniqueID(UniqueID, ArrID, Slot))
		{
			ISC->RemoveItemAt(ArrID);
			return true;
		}
	}
	return false;
}