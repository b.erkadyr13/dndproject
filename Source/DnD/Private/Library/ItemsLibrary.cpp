

#include "Library/ItemsLibrary.h"
#include "DataAssets/ItemData.h"
#include "Interface/ItemSystemInterface.h"

UItemData* UItemsLibrary::GetItemDataFromActor(AActor* Actor)
{
	if (auto ISI = Cast<IItemSystemInterface>(Actor))
	{
		return ISI->GetItemData();
	}
	return nullptr;
}