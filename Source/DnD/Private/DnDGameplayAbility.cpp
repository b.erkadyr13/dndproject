// Fill out your copyright notice in the Description page of Project Settings.


#include "DnDGameplayAbility.h"
#include "DnDCharacter.h"

void UDnDGameplayAbility::InitializeTarget()
{
	if (Cast<ADnDCharacter>(GetAvatarActorFromActorInfo()))
	{
		Target = Cast<ADnDCharacter>(GetAvatarActorFromActorInfo())->Target;
	}
}
