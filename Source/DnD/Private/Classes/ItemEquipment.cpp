

#include "Classes/ItemEquipment.h"

AItemEquipment::AItemEquipment(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
}

void AItemEquipment::OnEquipItem()
{
	K2_OnEquipItem();
}

void AItemEquipment::OnUnEquipItem()
{
	K2_OnUnEquipItem();
}