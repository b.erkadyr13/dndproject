

#include "Classes/ItemBase.h"
#include "DataAssets/ItemData.h"
#include "GameFramework/Pawn.h"

AItemBase::AItemBase(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

}

void AItemBase::BeginPlay()
{
	Super::BeginPlay();
	
}

bool AItemBase::OnCollect_Implementation(APawn* Causer)
{
	return false;
}

bool AItemBase::OnDrop_Implementation()
{
	return false;
}

UItemData* AItemBase::GetItemData() const
{
	return ItemData;
}