

#include "Classes/ItemUseProxyBase.h"
#include "Library/InventoryLibrary.h"

AItemUseProxyBase::AItemUseProxyBase(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	SetActorHiddenInGame(true);
}

void AItemUseProxyBase::BeginPlay()
{
	Super::BeginPlay();

	OnItemProxyUse.Broadcast(
		FItemProxy(
			OnItemUse()
		)
	);

	DELAY(0.166677f, &AItemUseProxyBase::RemoveProxyItem);
}

void AItemUseProxyBase::RemoveProxyItem()
{
	Destroy();
}