

#include "Classes/CameraPawn.h"

#include "Components/CameraMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Camera/CameraComponent.h"

#include "GameFramework/SpringArmComponent.h"

ACameraPawn::ACameraPawn(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;

	DefaultZoomLength = 600.0f;
	DefaultCameraRotation = FRotator(-50.0, 0.0, 0.0);

	// Set Root Component and also set root comps size.
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	SetRootComponent(CollisionSphere);
	CollisionSphere->InitSphereRadius(32.0);
	CollisionSphere->SetWorldScale3D(FVector(0.25, 0.25, 0.25));
	
	//Default Setting for inheriting controller rotations.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create CameraArm and attach to root
	CameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraArm"));
	CameraArm->SetupAttachment(RootComponent);
	CameraArm->TargetArmLength = DefaultZoomLength;
	CameraArm->SetRelativeRotation(DefaultCameraRotation); // Pitch (Y), Yaw (X), Roll (X)
	CameraArm->bDoCollisionTest = false;
	CameraArm->bEnableCameraLag = true; 
	CameraArm->bInheritPitch = false; // needs to be false for zoom to update in real time.

	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Player Camera"));
	PlayerCamera->SetupAttachment(CameraArm, USpringArmComponent::SocketName);

	CameraMovementComponent = CreateDefaultSubobject<UCameraMovementComponent>(TEXT("CamMovementComp"));
}

void ACameraPawn::BeginPlay()
{
	Super::BeginPlay();

	SetToDefaultZoom();
}

void ACameraPawn::SetArmLength(float ChangeAmount)
{
	CameraArm->TargetArmLength += ChangeAmount;
}

void ACameraPawn::SetArmRotation(FRotator ChangeAmount)
{
	// Setting our and max rotation amount
	const FRotator RotationMax = FRotator(-25.0, 0.0, 0.9); // Zoom in rotation max
	const FRotator RotationMin = DefaultCameraRotation; // Zoom out rotation min (same as default rotation)

	// Get "x", the rotation change
	FRotator NewRotation = FRotator(CameraArm->GetRelativeRotation() + ChangeAmount);

	// Clamp the pitch of NewRotation
	NewRotation = (NewRotation.Pitch < RotationMin.Pitch) ? RotationMin : (NewRotation.Pitch < RotationMax.Pitch) ? NewRotation : RotationMax;

	CameraArm->SetRelativeRotation(NewRotation);
}

void ACameraPawn::SetToDefaultZoom()
{
	CameraArm->TargetArmLength = DefaultZoomLength;
	CameraArm->SetRelativeRotation(DefaultCameraRotation);
}

