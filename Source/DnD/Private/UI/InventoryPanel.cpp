

#include "UI/InventoryPanel.h"
#include "UI/CardBase.h"

#include "Components/InventoryComponent.h"
#include "Components/GridPanel.h"
#include "Kismet/GameplayStatics.h"

void UInventoryPanel::NativePreConstruct()
{
	Super::NativePreConstruct();

	// Inventory preview in blueprints.
	//CreateInventoryUI(15);
}

void UInventoryPanel::NativeConstruct()
{
	Super::NativeConstruct();

	CreateInventoryUI(InventoryComponent);
}

void UInventoryPanel::SetInventoryRead(UInventoryComponent* const InventoryComp)
{
	CreateInventoryUI(InventoryComp);
	InventoryComponent = InventoryComp;
}

bool UInventoryPanel::CreateInventoryUI(int32 Slots)
{
	if (GP_Inventory && CardClass)
	{
		// Clear old card.
		ClearAllSlots();

		for (int32 i = 0; i < Slots; i++)
		{
			auto CardLocal = CreateWidget<UCardBase>(UGameplayStatics::GetPlayerController(this, 0), CardClass);
			Cards.Add(CardLocal);

			int32 r = i / SizeRow;
			if (r == 0)
			{
				GP_Inventory->AddChildToGrid(CardLocal, r, i);
			}
			else
			{
				int32 c = (r * SizeRow);
				GP_Inventory->AddChildToGrid(CardLocal, r, i - c);
			}
		}
		return true;
	}
	return false;
}

void UInventoryPanel::CreateInventoryUI(UInventoryComponent* const InventoryComp)
{
	if (InventoryComp)
	{
		UnBindFuncFromInventory(InventoryComponent);
		BindFuncFromInventory(InventoryComp);
		CreateInventoryUI(InventoryComp->GetAmountSlots());
		LoadSlots(InventoryComp);
	}
}

void UInventoryPanel::LoadSlots(UInventoryComponent* const FromInventoryComp)
{
	if (FromInventoryComp)
	{
		auto& InventoryLocal = FromInventoryComp->GetInventory();
		for (int32 i = 0; i < InventoryLocal.Num(); i++)
		{
			AddSlot(i, InventoryLocal[i]);
		}
	}
}

void UInventoryPanel::BindFuncFromInventory(UInventoryComponent* const InventoryComp)
{
	if (InventoryComp)
	{
		InventoryComp->OnAddItem.AddDynamic(this, &UInventoryPanel::OnAddedItem);
		InventoryComp->OnAddSlot.AddDynamic(this, &UInventoryPanel::OnAddedSlot);
		InventoryComp->OnRemovedItem.AddDynamic(this, &UInventoryPanel::OnRemovedItem);
		InventoryComp->OnRemovedSlot.AddDynamic(this, &UInventoryPanel::OnRemovedSlot);
	}
}

void UInventoryPanel::UnBindFuncFromInventory(UInventoryComponent* const InventoryComp)
{
	if (InventoryComp)
	{
		InventoryComp->OnAddItem.Remove(this, "OnAddedItem");
		InventoryComp->OnAddSlot.Remove(this, "OnAddedSlot");
		InventoryComp->OnRemovedItem.Remove(this, "OnRemovedItem");
		InventoryComp->OnRemovedSlot.Remove(this, "OnRemovedSlot");
	}
}

void UInventoryPanel::OnAddedItem(UItemData* NewItem, FInventorySlot InSlot)
{
	SetAmountItemAt(InSlot);
	
	OnAddedItemInInventory(NewItem, InSlot, InventoryComponent);
}

void UInventoryPanel::OnAddedSlot(FInventorySlot NewSlot)
{
	UCardBase* CardLocal;
	if (FindEmptyCard(CardLocal))
	{
		AddSlot(CardLocal, NewSlot);

		OnAddedSlotInInventory(NewSlot, InventoryComponent);
	}
}

void UInventoryPanel::OnRemovedItem(UItemData* RemovedItem, FInventorySlot InSlot)
{
	SetAmountItemAt(InSlot);

	OnRemovedItemInInventory(RemovedItem, InSlot, InventoryComponent);
}

void UInventoryPanel::OnRemovedSlot(FInventorySlot RemovedSlot)
{
	UCardBase* CardLocal;
	if (FindCardBySlot(RemovedSlot, CardLocal))
	{
		CardLocal->RemoveSlot();
		
		OnRemovedSlotInInventory(RemovedSlot, InventoryComponent);
	}
}

void UInventoryPanel::SetAmountItemAt(const FInventorySlot& InSlot, int32 NewAmount)
{
	UCardBase* CardLocal;
	if (FindCardBySlot(InSlot, CardLocal))
	{
		CardLocal->SetStack(NewAmount);
	}
}

void UInventoryPanel::AddSlot(int32 Index, const FInventorySlot& NewSlot)
{
	if (Cards.IsValidIndex(Index))
	{
		Cards[Index]->SetSlot(NewSlot);
	}
}

void UInventoryPanel::AddSlot(UCardBase* InCard, const FInventorySlot& NewSlot)
{
	if (Cards.Contains(InCard) != false)
	{
		InCard->SetSlot(NewSlot);
	}
}

void UInventoryPanel::SetAmountItemAt(const FInventorySlot& InSlot)
{
	SetAmountItemAt(InSlot, InSlot.Amount);
}

bool UInventoryPanel::FindCardBySlot(const FInventorySlot& BySlot, UCardBase*& CardPtr) const
{
	for (auto& CardLocal : Cards)
	{
		if (CardLocal)
		{
			if (CardLocal->GetSlot().UniqueID == BySlot.UniqueID)
			{
				CardPtr = CardLocal;
				return true;
			}
		}
	}

	return false;
}

bool UInventoryPanel::FindEmptyCard(UCardBase*& EmptyCard) const
{
	for (auto& CardLoc : Cards)
	{
		if ((CardLoc != nullptr) && (CardLoc->IsEmptyCard() != false))
		{
			EmptyCard = CardLoc;
			return true;
		}
	}
	return false;
}

void UInventoryPanel::ClearAllSlots()
{
	for (auto& Card : Cards)
	{
		if (Card)
		{
			Card->RemoveSlot();
		}
	}

	GP_Inventory->ClearChildren();
	Cards.Empty();
}