

#include "UI/CharacterPanel.h"

#include "CharactersAttributeSet.h"
#include "AbilitySystemComponent.h"

#include "GameplayEffectTypes.h"

void UCharacterPanel::SetAbilityRead(UAbilitySystemComponent* const ASC)
{
	BindFuncFromAbilityComponent(ASC);
	LoadStats(ASC);
}

void UCharacterPanel::BindFuncFromAbilityComponent(UAbilitySystemComponent* const ASC)
{
	if (ASC != nullptr)
	{
		if (auto AttributeBaseLocal = FindAttributeCharacter(ASC))
		{
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetHealthAttribute()).AddUObject(this, &UCharacterPanel::OnHealthChangeNative);
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetArmorAttribute()).AddUObject(this, &UCharacterPanel::OnArmorChangeNative);
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetAttackAttribute()).AddUObject(this, &UCharacterPanel::OnAttackChangeNative);
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetStrengthAttribute()).AddUObject(this, &UCharacterPanel::OnStrengthChangeNative);
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetAgilityAttribute()).AddUObject(this, &UCharacterPanel::OnAgilityChangeNative);
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetIntelligenceAttribute()).AddUObject(this, &UCharacterPanel::OnIntelligenceChangeNative);
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetVigorAttribute()).AddUObject(this, &UCharacterPanel::OnVigorChangeNative);
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetKnowledgeAttribute()).AddUObject(this, &UCharacterPanel::OnKnowledgeChangeNative);
			ASC->GetGameplayAttributeValueChangeDelegate(AttributeBaseLocal->GetSpeedAttribute()).AddUObject(this, &UCharacterPanel::OnSpeedChangeNative);
		}
	}
}

const UCharactersAttributeSet* UCharacterPanel::FindAttributeCharacter(UAbilitySystemComponent* const InASC)
{
	if (InASC)
	{
		if (auto AttributeLocal = InASC->GetAttributeSet(UCharactersAttributeSet::StaticClass()))
		{
			return Cast<UCharactersAttributeSet>(AttributeLocal);
		}
	}

	return nullptr;
}

void UCharacterPanel::LoadStats(UAbilitySystemComponent* const ASC)
{
	if (ASC != nullptr)
	{
		if (auto AttributeLocal = FindAttributeCharacter(ASC))
		{
			bool bLocal;
			OnHealthChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetHealthAttribute(), bLocal));
			OnArmorChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetArmorAttribute(), bLocal));
			OnAttackChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetAttackAttribute(), bLocal));
			OnStrengthChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetStrengthAttribute(), bLocal));
			OnAgilityChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetAgilityAttribute(), bLocal));
			OnIntelligenceChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetIntelligenceAttribute(), bLocal));
			OnVigorChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetVigorAttribute(), bLocal));
			OnKnowledgeChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetKnowledgeAttribute(), bLocal));
			OnSpeedChanged(0.0f, ASC->GetGameplayAttributeValue(AttributeLocal->GetSpeedAttribute(), bLocal));
		}
	}
}

void UCharacterPanel::OnHealthChangeNative(const FOnAttributeChangeData& Data)
{
	OnHealthChanged(Data.OldValue, Data.NewValue);
}

void UCharacterPanel::OnArmorChangeNative(const FOnAttributeChangeData& Data)
{
	OnArmorChanged(Data.OldValue, Data.NewValue);
}

void UCharacterPanel::OnAttackChangeNative(const FOnAttributeChangeData& Data)
{
	OnAttackChanged(Data.OldValue, Data.NewValue);
}

void UCharacterPanel::OnStrengthChangeNative(const FOnAttributeChangeData& Data)
{
	OnStrengthChanged(Data.OldValue, Data.NewValue);
}

void UCharacterPanel::OnAgilityChangeNative(const FOnAttributeChangeData& Data)
{
	OnAgilityChanged(Data.OldValue, Data.NewValue);
}

void UCharacterPanel::OnIntelligenceChangeNative(const FOnAttributeChangeData& Data)
{
	OnIntelligenceChanged(Data.OldValue, Data.NewValue);
}

void UCharacterPanel::OnVigorChangeNative(const FOnAttributeChangeData& Data)
{
	OnVigorChanged(Data.OldValue, Data.NewValue);
}

void UCharacterPanel::OnKnowledgeChangeNative(const FOnAttributeChangeData& Data)
{
	OnKnowledgeChanged(Data.OldValue, Data.NewValue);
}

void UCharacterPanel::OnSpeedChangeNative(const FOnAttributeChangeData& Data)
{
	OnSpeedChanged(Data.OldValue, Data.NewValue);
}
