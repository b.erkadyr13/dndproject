

#include "UI/CardBase.h"
#include "DataAssets/ItemData.h"

#include "Components/Image.h"
#include "Components/TextBlock.h"

#include "Kismet/KismetTextLibrary.h"

void UCardBase::NativeConstruct()
{
	Super::NativeConstruct();

	SetSlot(InventorySlot);
}

void UCardBase::SetSlot(const FInventorySlot& NewSlot)
{
	if (OnPreLoadedSlot(NewSlot))
	{
		InventorySlot = NewSlot;

		if (InitCard())
		{
			OnLoadedSlot(InventorySlot);
		}
	}
}

bool UCardBase::InitCard()
{
	if ((InventorySlot.ItemData != nullptr) && (I_Icon != nullptr))
	{
		I_Icon->SetBrushFromSoftTexture(InventorySlot.ItemData->ItemIcon);
		I_Icon->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
		SetStack(InventorySlot.Amount);

		bEmptyCard = false;

		return true;
	}
	return false;
}

void UCardBase::SetStack(int32 NewStack)
{
	TB_Amount->SetVisibility((NewStack > 1) ? ESlateVisibility::SelfHitTestInvisible : ESlateVisibility::Hidden);
	TB_Amount->SetText(UKismetTextLibrary::Conv_IntToText(NewStack));

	OnValueChangeStack(NewStack);
}

void UCardBase::RemoveSlot()
{
	I_Icon->SetVisibility(ESlateVisibility::Hidden);
	I_Icon->SetBrushFromTexture(nullptr);

	SetStack(0);

	auto SlotLocal = InventorySlot;
	InventorySlot.ClearSlot();
	bEmptyCard = true;
	
	OnRemovedSlot(SlotLocal);
}