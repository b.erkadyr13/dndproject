

#include "DnDCharacter.h"

#include "Components/InventoryComponent.h"
#include "Components/EquipmentComponent.h"

ADnDCharacter::ADnDCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilityComponent"));
	
	InventorySystemComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("ISC"));
	EquipmentSystemComponent = CreateDefaultSubobject<UEquipmentComponent>(TEXT("ESC"));
}

// Called when the game starts or when spawned
void ADnDCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (AbilitySystemComponent)
	{
		AttributeComponent = AbilitySystemComponent->GetSet<UCharactersAttributeSet>();
	}
}

// Called every frame
void ADnDCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
UAbilitySystemComponent* ADnDCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}
void ADnDCharacter::GiveAbility(UDnDAbilityDataAsset* Ability) {
	InitializeAbility(Ability->Ability, 1);
}
void ADnDCharacter::AssignAbilitiesFromSet()
{
	NumberOfAbilities = AbilitySet->Abilities.Num();
	for (int i = 0; i < AbilitySet->Abilities.Num(); i++)
	{
		GiveAbility(AbilitySet->Abilities[i]);
	}
}
void ADnDCharacter::InitializeAbility(TSubclassOf<UGameplayAbility> Ability, int32 AbilityLvl)
{
	if (AbilitySystemComponent)
	{
		if (HasAuthority() && Ability) {
			AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(Ability, AbilityLvl, 0));
		}
		AbilitySystemComponent->InitAbilityActorInfo(this, this);
	}
}

UInventoryComponent* ADnDCharacter::GetInventorySystemComponent() const
{
	return InventorySystemComponent;
}

UEquipmentComponent* ADnDCharacter::GetEquipmentSystemComponent() const
{
	return EquipmentSystemComponent;
}