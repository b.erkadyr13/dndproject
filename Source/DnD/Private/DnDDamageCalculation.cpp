// Fill out your copyright notice in the Description page of Project Settings.


#include "DnDDamageCalculation.h"
UENUM()

struct FDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Strength);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Armor);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Attack);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Intelligence);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Knowledge);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Health);

	FDamageStatics() {
		DEFINE_ATTRIBUTE_CAPTUREDEF(UCharactersAttributeSet, Strength, Source, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UCharactersAttributeSet, Armor, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UCharactersAttributeSet, Attack, Source, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UCharactersAttributeSet, Intelligence, Source, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UCharactersAttributeSet, Knowledge, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UCharactersAttributeSet, Health, Target, false);
	}
};
static const FDamageStatics& DamageStatics() {
	static FDamageStatics DmgStatics;
	return DmgStatics;
}


UDnDDamageCalculation::UDnDDamageCalculation() {
	RelevantAttributesToCapture.Add(DamageStatics().StrengthDef);
	RelevantAttributesToCapture.Add(DamageStatics().IntelligenceDef);
	RelevantAttributesToCapture.Add(DamageStatics().KnowledgeDef);
	RelevantAttributesToCapture.Add(DamageStatics().ArmorDef);
	RelevantAttributesToCapture.Add(DamageStatics().AttackDef);
	RelevantAttributesToCapture.Add(DamageStatics().HealthDef);
}
void UDnDDamageCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const {
	UAbilitySystemComponent* TargetDnD = ExecutionParams.GetTargetAbilitySystemComponent();
	AActor* TargetActor = TargetDnD ? TargetDnD->GetAvatarActor() : nullptr;

	UAbilitySystemComponent* SourceDnD = ExecutionParams.GetSourceAbilitySystemComponent();
	AActor* SourceActor = SourceDnD ? SourceDnD->GetAvatarActor() : nullptr;

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float BaseDamage = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().AttackDef, EvaluationParameters, BaseDamage);

	float PhysicResistance = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().ArmorDef, EvaluationParameters, PhysicResistance);

	float PhysicDamageModifier = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().StrengthDef, EvaluationParameters, PhysicDamageModifier);

	float MagicDamageModifier = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().IntelligenceDef, EvaluationParameters, MagicDamageModifier);

	float MagicResistance = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().KnowledgeDef, EvaluationParameters, MagicResistance);
	float DamageDone = 0.0f;
	
	switch (DamageType)
	{
	case EDamageType::Magic:
		DamageDone = ((BaseDamage + (MagicDamageModifier * DamageModifierMultiplier)) * (1 - ((MagicResistance * ResistanceModifierMultiplier) / 100)));
		break;
	case EDamageType::Physical:
		DamageDone = ((BaseDamage + (PhysicDamageModifier * DamageModifierMultiplier))) * (1 - ((PhysicResistance * ResistanceModifierMultiplier) / 100));
		break;
	default:
		break;
	}
	
	if (DamageDone < 0.0f)
	{
		DamageDone = 0.0f;
	}

	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DamageStatics().HealthProperty, EGameplayModOp::Additive, -DamageDone));
}
