

#include "Components/InventoryComponent.h"
#include "Interface/InventorySystemInterface.h"

#include "DataAssets/ItemData.h"

#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

UInventoryComponent::UInventoryComponent(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = false;

}

void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}

bool UInventoryComponent::AddItem(UItemData* Item)
{
	if (Item)
	{
		int32 ArrID;
		FInventorySlot Slot;
		if (FindSlotByItem(Item, ArrID, Slot, true)) // Find an existing element with extra space.
		{
			// Just increase one item.
			Inventory[ArrID].Amount = Slot.Amount + 1;
		}
		else if (IsEmptySlot()) // If not found existing element else check empty slot in inventory.
		{
			// Add item in array.
			ArrID = Inventory.Add(
				FInventorySlot(
					GenerateUniqueID(1, 9999),  // UniqID.
					Item,                       // ItemAsset.
					1                           // Default Amount.
				)
			);

			OnAddSlot.Broadcast(Inventory[ArrID]);
		}
		else
		{
			return false;
		}

		OnAddItem.Broadcast(Item, Inventory[ArrID]);
		return true;
	}

	return false;
}

bool UInventoryComponent::RemoveItemAt(int32 Index)
{
	if (Inventory.IsValidIndex(Index))
	{
		// Dicrement one item from slot.
		Inventory[Index].Amount -= 1;

		OnRemovedItem.Broadcast(
			Inventory[Index].ItemData,
			Inventory[Index]
		);

		if (Inventory[Index].Amount == 0)
		{
			RemoveSlotAt(Index);
		}
		
		return true;
	}
	return false;
}

bool UInventoryComponent::RemoveSlotAt(int32 Index)
{
	if (Inventory.IsValidIndex(Index))
	{
		OnRemovedSlot.Broadcast(Inventory[Index]);
		Inventory.RemoveAt(Index);
		return true;
	}
	return false;
}

bool UInventoryComponent::IsEmptySlot() const
{
	for (int32 i = 0; i < AmountSlots; i++)
	{
		if (IsFreeSlot(i))
		{
			return true;
		}
	}

	return false;
}

bool UInventoryComponent::FindSlotByItem(UItemData* Item, int32& ArrayID, FInventorySlot& Slot, bool bSkipFullSlot) const
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i].ItemData == Item)
		{
			if (bSkipFullSlot)
			{
				if (IsFullSlot(i))
				{
					continue;
				}
			}

			ArrayID = i;
			Slot = Inventory[i];
			return true;
		}
	}
	return false;
}

bool UInventoryComponent::FindSlotByUniqueID(int32 UniqueID, int32& ArrayID, FInventorySlot& Slot) const
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i].UniqueID == UniqueID)
		{
			ArrayID = i;
			Slot = Inventory[i];
			return true;
		}
	}
	return false;
}

bool UInventoryComponent::IsFreeSlot(int32 ID) const
{
	return (ID < AmountSlots) ? (Inventory.IsValidIndex(ID) != true) : false;
}

bool UInventoryComponent::IsFullSlot(int32 ID) const
{
	if (!IsFreeSlot(ID))
	{
		const int32 MaxStack = Inventory[ID].ItemData->ItemMaxStack;
		if (Inventory[ID].Amount >= MaxStack)
		{
			return true;
		}
	}
	return false;
}

int32 UInventoryComponent::CountItems(UItemData* Item) const
{
	int32 CountLocal = 0;
	for (auto& slot : Inventory)
	{
		if (slot.ItemData == Item)
		{
			CountLocal = slot.Amount + CountLocal;
		}
	}
	return CountLocal;
}

int32 UInventoryComponent::GenerateUniqueID(int32 MinID, int32 MaxID)
{
	if (AmountSlots > Inventory.Num())
	{
		int32 UniqID;
		int32 ArrID;
		FInventorySlot Slot;

		do
		{
			UniqID = UKismetMathLibrary::RandomIntegerInRange(MinID, MaxID);

		} while (FindSlotByUniqueID(UniqID, ArrID, Slot));

		return UniqID;
	}

	return -1;
}