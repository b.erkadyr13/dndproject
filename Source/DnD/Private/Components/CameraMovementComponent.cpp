

#include "Components/CameraMovementComponent.h"
#include "Components/SceneComponent.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"

#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

UCameraMovementComponent::UCameraMovementComponent(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;

	DefaultMovementSpeed = 40.0f;
	MovementSpeedModifier = 1.0f;
	Threshold = 1000;
	HeightAdjustmentSmoothness = 7.0f;
	MaxZoom = 300.0f;
	MinZoom = 1000.0f; 
	DeltaArm = 35.0f;
	bDisableCameraMovement = false;
	bDisableCameraZoom = false;
	bHeightAdjustment = true;
	bEdgeScroll = true;
}

void UCameraMovementComponent::BeginPlay()
{
	Super::BeginPlay();
	
	ReceivePlayerController();
}

void UCameraMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	EdgeScroll(DeltaTime);
	AdjustHeight(DeltaTime);
}

void UCameraMovementComponent::ReceivePlayerController()
{
	auto ControllerLocal = GetOwner()->GetInstigatorController();
	PlayerControllerPtr = Cast<APlayerController>(ControllerLocal);
}

void UCameraMovementComponent::AdjustHeight(float DeltaTime)
{
	if (bHeightAdjustment)
	{
		float TargetHeight;
		int32 PreviosHeight;
		CalculateTargetHeight(0.0f, TargetHeight, PreviosHeight);
		if (GetCameraLocation().Z != TargetHeight)
		{
			const float HeightToSet = UKismetMathLibrary::FInterpTo(
				GetCameraLocation().Z,
				TargetHeight,
				DeltaTime,
				HeightAdjustmentSmoothness
			);
			
			// Set new location.
			GetOwner()->SetActorLocation(FVector(
				GetCameraLocation().X,
				GetCameraLocation().Y,
				HeightToSet));
		}
	}
}

void UCameraMovementComponent::CalculateTargetHeight(float StartHeight, float& TargetHeight, int32& PreviousHeight)
{
	FHitResult HitResult;
	FVector Start = GetCameraLocation();
	FVector End = GetCameraLocation();

	Start.Z = 999999;
	End.Z = -999999;

	FCollisionObjectQueryParams QueryParams;
	QueryParams.AddObjectTypesToQuery(TraceChannel);
	GetWorld()->LineTraceSingleByObjectType(HitResult, Start, End, QueryParams);

	if (UKismetMathLibrary::Abs(PreviousHeight - HitResult.Location.Z) >= Threshold)
	{
		TargetHeight = HitResult.Location.Z;
		PreviousHeight = HitResult.Location.Z;
	}
	else if (UKismetMathLibrary::Abs(HitResult.Location.Z - StartHeight) < Threshold)
	{
		TargetHeight = StartHeight;
	}
}

void UCameraMovementComponent::EdgeScroll(float DeltaSecond)
{
	if ((PlayerControllerPtr == nullptr) || (bEdgeScroll != true)) { return; }

	// Variebles for movement.
	float DeltaSpeedX;
	float DeltaSpeedY;
	FVector MovementX; //use with DeltaSpeedY.
	FVector MovementY; // use with DeltaSpeedX.

	// Get Mouse Position.
	float MousePositionX{ 0 };
	float MousePositionY{ 0 };
	PlayerControllerPtr->GetMousePosition(MousePositionX, MousePositionY);

	// Get Viewport (play screen) size.
	int32 ViewportSizeX;
	int32 ViewportSizeY;
	PlayerControllerPtr->GetViewportSize(ViewportSizeX, ViewportSizeY);

	//Calculate proportion (we what these calculations to be updated with the mouses movement).
	float ProportionX = MousePositionX / ViewportSizeX;
	float ProportionY = MousePositionY / ViewportSizeY;

	// Set ScrollSpeeds base on Proportion.
	if (ProportionX >= .975 && ProportionY <= .025) // TOP RIGHT corner.
	{ 
		DeltaSpeedX = (DefaultMovementSpeed + 10.0f) * GetModifierSpeed();
		DeltaSpeedY = (DefaultMovementSpeed + 10.0f) * GetModifierSpeed();

		MovementX = FVector(DeltaSpeedY, 0.0f, 0.0f) * DeltaSecond;
		MovementY = FVector(0.0f, DeltaSpeedX, 0.0f) * DeltaSecond;

		GetOwner()->AddActorLocalOffset(MovementX + MovementY, true);
		SetCameraDisable(true);
	}
	else if (ProportionX >= .975 && ProportionY >= .975) // BOTTOM RIGHT.
	{
		DeltaSpeedX = (DefaultMovementSpeed + 10.0f) * GetModifierSpeed();
		DeltaSpeedY = (-DefaultMovementSpeed - 10.0f) * GetModifierSpeed();

		MovementX = FVector(DeltaSpeedY, 0.0f, 0.0f) * DeltaSecond;
		MovementY = FVector(0.0f, DeltaSpeedX, 0.0f) * DeltaSecond;

		GetOwner()->AddActorLocalOffset(MovementX + MovementY, true);
		SetCameraDisable(true);
	}
	else if (ProportionX <= .025 && ProportionY <= .025) //TOP LEFT.
	{ 
		DeltaSpeedX = (-DefaultMovementSpeed - 10.0f) * GetModifierSpeed();
		DeltaSpeedY = (DefaultMovementSpeed + 10.0f) * GetModifierSpeed();

		MovementX = FVector(DeltaSpeedY, 0.0f, 0.0f) * DeltaSecond;
		MovementY = FVector(0.0f, DeltaSpeedX, 0.0f) * DeltaSecond;

		GetOwner()->AddActorLocalOffset(MovementX + MovementY, true);
		SetCameraDisable(true);
	}
	else if (ProportionX <= .025 && ProportionY >= .975) { // BOTTOM LEFT.
		DeltaSpeedX = (-DefaultMovementSpeed + 10.0f) * GetModifierSpeed();
		DeltaSpeedY = (-DefaultMovementSpeed - 10.0f) * GetModifierSpeed();

		MovementX = FVector(DeltaSpeedY, 0.0f, 0.0f) * DeltaSecond;
		MovementY = FVector(0.0f, DeltaSpeedX, 0.0f) * DeltaSecond;

		GetOwner()->AddActorLocalOffset(MovementX + MovementY, true);
		SetCameraDisable(true);
	}
	else if (ProportionX >= .995) { // FAR RIGHT.
		DeltaSpeedX = (DefaultMovementSpeed * GetModifierSpeed());
		DeltaSpeedY = 0;

		MovementY = FVector(0.0f, DeltaSpeedX, 0.0f) * DeltaSecond;

		GetOwner()->AddActorLocalOffset(MovementY, true);
		SetCameraDisable(true);
	}
	else if (ProportionX <= .005) { // FAR LEFT.
		DeltaSpeedX = -DefaultMovementSpeed * GetModifierSpeed();
		DeltaSpeedY = 0;

		MovementY = FVector(0.0f, DeltaSpeedX, 0.0f)  * DeltaSecond;

		GetOwner()->AddActorLocalOffset(MovementY, true);
		SetCameraDisable(true);
	}
	else if (ProportionY >= .995) { // BOTTOM.
		DeltaSpeedX = 0;
		DeltaSpeedY = -DefaultMovementSpeed * GetModifierSpeed();

		MovementY = FVector(DeltaSpeedY, 0.0f, 0.0f)  * DeltaSecond;

		GetOwner()->AddActorLocalOffset(MovementY, true);
		SetCameraDisable(true);
	}
	else if (ProportionY <= .005) { // TOP.
		DeltaSpeedX = 0;
		DeltaSpeedY = DefaultMovementSpeed * GetModifierSpeed();

		MovementY = FVector(DeltaSpeedY, 0.0f, 0.0f) * DeltaSecond;

		GetOwner()->AddActorLocalOffset(MovementY, true);
		SetCameraDisable(true);
	}
	else 
	{
		DeltaSpeedX = 0;
		DeltaSpeedY = 0;

		SetCameraDisable(false);
	}
}

bool UCameraMovementComponent::SetCameraDisable(bool bDisableCamera)
{
	return bDisableCameraMovement = bDisableCamera;
}

void UCameraMovementComponent::BasicMovementControl(float AxisValueX, float AxisValueY)
{
	// Set Variables used in movement calculation
	float MovementValueX;
	float MovementValueY;
	FVector MovementX;
	FVector MovementY;

	if (IsCameraDisable() != true) 
	{
		const float DeltaSecond = UGameplayStatics::GetWorldDeltaSeconds(GetOwner());

		if (AxisValueX != 0 && AxisValueY != 0) 
		{
			MovementValueX = (AxisValueX * GetDefaultSpeed() * GetModifierSpeed()) * DeltaSecond;
			MovementX = FVector(MovementValueX, 0, 0);

			MovementValueY = (AxisValueY * GetDefaultSpeed() * GetModifierSpeed()) * DeltaSecond;
			MovementY = FVector(0, MovementValueY, 0);

			GetOwner()->AddActorLocalOffset(MovementX + MovementY, true);

		}
		else if (AxisValueX != 0) 
		{
			MovementValueX = (AxisValueX * GetDefaultSpeed() * GetModifierSpeed()) * DeltaSecond;
			MovementX = FVector(MovementValueX, 0, 0);

			GetOwner()->AddActorLocalOffset(MovementX, true);
		}
		else if (AxisValueY != 0) 
		{
			MovementValueY = (AxisValueY * GetDefaultSpeed() * GetModifierSpeed()) * DeltaSecond;
			MovementY = FVector(0, MovementValueY, 0);

			GetOwner()->AddActorLocalOffset(MovementY, true);
		}
	}
}

void UCameraMovementComponent::PanCamera(float RotationAmount)
{
	RotationAmount = RotationAmount * 1.5f;
	FRotator NewRotation = GetCameraRotation().Add(0, RotationAmount, 0);
	GetOwner()->SetActorRotation(NewRotation);
}

void UCameraMovementComponent::ResetPan()
{
	GetOwner()->SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));
}

void UCameraMovementComponent::ZoomIn()
{
	if (auto SpringArm = GetSpringArm())
	{
		float CurrentZoom = SpringArm->TargetArmLength;;
		float ReturnZoom = FMath::Clamp(CurrentZoom, MaxZoom, MinZoom);

		if (ReturnZoom != MaxZoom)
		{
			SpringArm->TargetArmLength = CurrentZoom + (DeltaArm * (-1));
		}
	}
}

void UCameraMovementComponent::ZoomOut()
{
	if (auto SpringArm = GetSpringArm())
	{
		float CurrentZoom = SpringArm->TargetArmLength;;
		float ReturnZoom = FMath::Clamp(CurrentZoom, MaxZoom, MinZoom);

		if (ReturnZoom != MinZoom)
		{
			SpringArm->TargetArmLength = CurrentZoom + DeltaArm;
		}
	}
}

USpringArmComponent* UCameraMovementComponent::GetSpringArm() const
{
	return GetOwner()->FindComponentByClass<USpringArmComponent>();
}