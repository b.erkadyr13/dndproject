

#include "Components/EquipmentComponent.h"
#include "Classes/ItemEquipment.h"

#include "DataAssets/ItemData.h"

UEquipmentComponent::UEquipmentComponent(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UEquipmentComponent::BeginPlay()
{
	Super::BeginPlay();
}

bool UEquipmentComponent::AddEquipment(EEquipment InSlot, AItemEquipment* Equip)
{
	if ((InSlot != EEquipment::EE_None) && (CheckEqualSlot(InSlot, Equip) != false))
	{
		// Remove old equip if he valid.
		RemoveEquip(InSlot);
		Equipments.Add(InSlot, Equip);

		Equip->SetOwner(GetOwner());
		Equip->OnEquipItem();

		OnAddEquip.Broadcast(
			FEquipManager(
				Equip->GetItemData()
			)
		);
		return true;
	}

	return false;
}

bool UEquipmentComponent::RemoveEquip(EEquipment FromSlot)
{
	AItemEquipment* Equip;
	if (GetEquipFromSlot(FromSlot, Equip))
	{
		// Remove ref equip from MAP.
		Equipments.Remove(FromSlot);

		Equip->OnUnEquipItem();
		Equip->SetOwner(nullptr);

		OnRemovedEquip.Broadcast(
			FEquipManager(
				Equip->GetItemData()
			)
		);

		Equip->Destroy();

		return true;
	}

	return false;
}

bool UEquipmentComponent::GetEquipFromSlot(EEquipment FromSlot, AItemEquipment*& EquipRef) const
{
	if (Equipments.Contains(FromSlot))
	{
		EquipRef = Equipments.FindRef(FromSlot);
		return true;
	}

	return false;
}

bool UEquipmentComponent::CheckEqualSlot(EEquipment InSlot, AItemEquipment* Equip)
{

	return (Equip != nullptr) ? (Equip->GetSlotForEquip() == InSlot) : false;
}
