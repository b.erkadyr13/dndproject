// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GameplayEffectExecutionCalculation.h"
#include "CharactersAttributeSet.h"
#include "AttributeSet.h"
#include "DnDDamageCalculation.generated.h"
UENUM(BlueprintType)
enum class EDamageType
{
	Magic UMETA(DisplayName = "Magic"),
	Physical UMETA(DisplayName = "Physical")
};
/**
 * 
 */
UCLASS()
class DND_API UDnDDamageCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:
	UPROPERTY()
	FGameplayAttribute Attack;
	UPROPERTY()
	FGameplayAttribute Strength;
	UPROPERTY(EditAnywhere)
	float DamageModifierMultiplier;
	UPROPERTY()
	FGameplayAttribute Armor;
	UPROPERTY(EditAnywhere)
	float ResistanceModifierMultiplier;
	UPROPERTY()
	FGameplayAttribute Health;
	UPROPERTY()
	FGameplayAttribute Intelligence;
	UPROPERTY()
	FGameplayAttribute Knowledge;
	
	UPROPERTY(EditAnywhere)
	EDamageType DamageType;
	UDnDDamageCalculation();

	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

	void InitializeAttributes(FGameplayAttribute Attribute);
};
