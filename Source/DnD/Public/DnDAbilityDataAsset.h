// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "Engine/DataAsset.h"
#include "Components/Image.h"
#include "DnDGameplayAbility.h"
#include "DnDAbilityDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class DND_API UDnDAbilityDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString AbilityName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<UDnDGameplayAbility> Ability;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UTexture2D*AbilityIcon;
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	int AbilityCD;
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	int AbilityCCD;
};
