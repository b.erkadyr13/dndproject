
#pragma once

#include "CoreMinimal.h"
#include "Classes/ItemBase.h"
#include "Components/EquipmentComponent.h"
#include "ItemEquipment.generated.h"


/**
 * 
 */
UCLASS(Abstract)
class DND_API AItemEquipment : public AItemBase
{
	GENERATED_BODY()
	
public:
	AItemEquipment(const FObjectInitializer& ObjectInitializer);

public:
	/** Called when item is equipped or unequipped. */
	virtual void OnEquipItem();
	virtual void OnUnEquipItem();

public:
	/** Return slot for equip item. */
	FORCEINLINE const EEquipment& GetSlotForEquip() const { return InSlot; }

protected:
	/** Called when item is equipped in BP. */
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "On Equip Item"))
	void K2_OnEquipItem();

	/** Called when item is unequipped in BP. */
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "On UnEquip Item"))
	void K2_OnUnEquipItem();

private:
	/** Slot for equip item. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Item Base|Equipment")
	EEquipment InSlot = EEquipment::EE_None;
};
