
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InventoryComponent.h"
#include "ItemUseProxyBase.generated.h"

USTRUCT(BlueprintType)
struct FItemProxy
{
	GENERATED_USTRUCT_BODY()

public:
	FItemProxy()
	{}

	FItemProxy(bool IsSuccessfully) 
		: bSuccessfully(IsSuccessfully)
	{}

public:
	UPROPERTY(BlueprintReadOnly)
	bool bSuccessfully;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemProxyUse, FItemProxy, ItemUse);

/** ����� ������� � ��������� ��� ����������. */
#define DELAY(dealy, function) \
    FTimerHandle __delayTimerHandle; \
    GetWorld()->GetTimerManager().SetTimer(__delayTimerHandle, this, function, (dealy), false);

/** ����� ������� � ��������� � ����� ����������. */
#define DELAY_OneParam(delay, function, value) \
    FTimerHandle __delayTimerHandleOneParam; \
    FTimerDelegate __delayTimerDelegateOneParam = FTimerDelegate::CreateUObject(this, function, (value)); \
    GetWorld()->GetTimerManager().SetTimer(__delayTimerHandleOneParam, __delayTimerDelegateOneParam, (delay), false);

/**
* Runs the logic for using an item. After use, this actor proxy is deleted.
*/
UCLASS(Abstract)
class DND_API AItemUseProxyBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AItemUseProxyBase(const FObjectInitializer& ObjectInitializer);

public:
	/** Called when an item is used. */
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta = (DisplayName = "On Item Use"))
	FOnItemProxyUse OnItemProxyUse;

protected:
	virtual void BeginPlay() override;

protected:
	/** Function responsible for using the item. */
	UFUNCTION(BlueprintImplementableEvent, Category = "Item Proxy")
	bool OnItemUse();

private:
	/** Called after using an item. */
	void RemoveProxyItem();

private:
	/** Item Asset. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true"), Category = "Private")
	FInventorySlot ItemAsset;
};
