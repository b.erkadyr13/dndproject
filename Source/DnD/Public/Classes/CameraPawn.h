
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CameraPawn.generated.h"

UCLASS()
class DND_API ACameraPawn : public APawn
{
	GENERATED_BODY()

public:
	ACameraPawn(const FObjectInitializer& ObjectInitializer);

public:
	/***/
	UFUNCTION(BlueprintCallable, Category = "Camera Zoom")
	void SetArmLength(float ChangeAmount);

	/***/
	UFUNCTION(BlueprintCallable, Category = "Camera Zoom")
	void SetArmRotation(FRotator ChangeAmount);

	/***/
	UFUNCTION(BlueprintCallable, Category = "Camera Zoom")
	void SetToDefaultZoom();

protected:
	virtual void BeginPlay() override;

private:
	/** Collision Sphere and root component. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Component")
	TObjectPtr<class USphereComponent> CollisionSphere;

	/** Player Camera. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Component")
	TObjectPtr<class UCameraComponent> PlayerCamera;

	/** SpringArm or CameraBoom. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Component")
	TObjectPtr<class USpringArmComponent> CameraArm;

	/** CameraMovement. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Component")
	TObjectPtr<class UCameraMovementComponent> CameraMovementComponent;

private:
	/* Default CameraArm Length. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Camera|Zoom")
	float DefaultZoomLength;

	/* Default Rotation. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Camera|Zoom")
	FRotator DefaultCameraRotation;
};
