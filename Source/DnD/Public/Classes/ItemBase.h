
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/ItemSystemInterface.h"
#include "ItemBase.generated.h"

UCLASS(Abstract)
class DND_API AItemBase : public AActor, public IItemSystemInterface
{
	GENERATED_BODY()
	
public:	
	AItemBase(const FObjectInitializer& ObjectInitializer);

public:
	virtual bool OnCollect_Implementation(APawn* Causer) override;
	virtual bool OnDrop_Implementation() override;
	virtual UItemData* GetItemData() const override;

protected:
	virtual void BeginPlay() override;

private:
	/** Item Data. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true"), Category = "Item Base")
	TObjectPtr<class UItemData> ItemData = nullptr;

};
