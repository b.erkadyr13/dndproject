
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ItemData.generated.h"

UENUM(BlueprintType)
enum class EItemType : uint8
{
	IT_Equipment        UMETA(DisplayName = "Equipment"),
	IT_Resources        UMETA(DisplayName = "Resources"),
	IT_Consumable       UMETA(DisplayName = "Consumable")
};

/**
 * 
 */
UCLASS()
class DND_API UItemData : public UPrimaryDataAsset
{
	GENERATED_BODY()
	
public:
	/** Unique item name. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setting")
	FName ItemName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setting")
	FText ItemDisplayName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setting")
	FText ItemDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setting")
	TSoftObjectPtr<UTexture2D> ItemIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setting")
	TSoftClassPtr<AActor> ItemActorClassAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setting")
	TSoftClassPtr<class AItemUseProxyBase> ItemUseProxyClassAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Setting")
	EItemType ItemType = EItemType::IT_Consumable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 1), Category = "Item Setting")
	int32 ItemMaxStack = 1;
};
