// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DnDAbilityDataAsset.h"
#include "DnDAbilitySet.generated.h"

/**
 * 
 */
UCLASS()
class DND_API UDnDAbilitySet : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UDnDAbilityDataAsset*> Abilities;
};
