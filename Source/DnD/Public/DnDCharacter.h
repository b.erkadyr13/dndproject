
#pragma once

#include "CoreMinimal.h"
#include "Interface/InventorySystemInterface.h"
#include "Interface/EquipmentSystemInterface.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "Abilities/GameplayAbility.h"

#include "GameFramework/Character.h"
#include "CharactersAttributeSet.h"

#include "DnDAbilityDataAsset.h"
#include "DnDAbilitySet.h"
#include "DnDCharacter.generated.h"
class UCharactersAttributeSet;

UCLASS()
class DND_API ADnDCharacter : public ACharacter, public IAbilitySystemInterface, public IInventorySystemInterface, public IEquipmentSystemInterface
{
	GENERATED_BODY()

public:
	ADnDCharacter();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AbilitySystem")
	UAbilitySystemComponent* AbilitySystemComponent;

	UFUNCTION(BlueprintCallable, Category = "AbilitySystem")
	void InitializeAbility(TSubclassOf<UGameplayAbility> Ability, int32 AbilityLvl);

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	const class UCharactersAttributeSet* AttributeComponent;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	UDnDAbilitySet* AbilitySet;

	UFUNCTION()
	void GiveAbility(UDnDAbilityDataAsset* Ability);

	UFUNCTION(BlueprintCallable)
	void AssignAbilitiesFromSet();

	UPROPERTY(BlueprintReadOnly)
	int NumberOfAbilities;

	UPROPERTY(BlueprintReadWrite)
	class ADnDCharacter* Target;

public:
	/** Return inventory component a character. */
	virtual UInventoryComponent* GetInventorySystemComponent() const override;

	/** Return equipment component a character. */
	virtual UEquipmentComponent* GetEquipmentSystemComponent() const override;

private:
	/** Character inventory. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Component")
	TObjectPtr<class UInventoryComponent> InventorySystemComponent;

	/** Character equipment. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Component")
	TObjectPtr<class UEquipmentComponent> EquipmentSystemComponent;
};
