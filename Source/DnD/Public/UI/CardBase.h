
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/InventoryComponent.h"
#include "CardBase.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class DND_API UCardBase : public UUserWidget
{
	GENERATED_BODY()

public:
	/** Set new item on a slot. */
	UFUNCTION(BlueprintCallable, Category = "Inventory Slot")
	void SetSlot(const FInventorySlot& NewSlot);

	/** Set new value stack items. */
	void SetStack(int32 NewStack);

	/** Removes a slot from a card. */
	UFUNCTION(BlueprintCallable, Category = "Inventory Slot")
	void RemoveSlot();

public:
	/** Return inventory slot. */
	FORCEINLINE const FInventorySlot& GetSlot() const { return InventorySlot; }

	/** Return true, if slot in card empty. */
	FORCEINLINE bool IsEmptyCard() const { return (bool)bEmptyCard; }

protected:
	virtual void NativeConstruct() override;

protected:
	/** Called when loaded slot. */
	UFUNCTION(BlueprintImplementableEvent, Category = "Inventory Slot")
	void OnLoadedSlot(const FInventorySlot& NewSlot);

	/**  */
	UFUNCTION(BlueprintImplementableEvent, Category = "Inventory Slot")
	bool OnPreLoadedSlot(const FInventorySlot& NewSlot);

	/** Called when removes a slot from card. */
	UFUNCTION(BlueprintImplementableEvent, Category = "Inventory Slot")
	void OnRemovedSlot(const FInventorySlot& OldSlot);

	/** Called when updated stack slot. */
	UFUNCTION(BlueprintImplementableEvent, Category = "Inventory Slot")
	void OnValueChangeStack(int32 NewStack);

protected:
	/** Icon item. */
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	TObjectPtr<class UImage> I_Icon;

	/** Text amount item. */
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	TObjectPtr<class UTextBlock> TB_Amount;

private:
	bool InitCard();

private:
	/***/
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true"), Category = "Card UI")
	FInventorySlot InventorySlot;

	/***/
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Card UI")
	uint8 bEmptyCard : 1 = true;
	
};
