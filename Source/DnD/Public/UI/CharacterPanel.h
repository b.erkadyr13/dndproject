
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameplayEffectTypes.h"
#include "CharacterPanel.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class DND_API UCharacterPanel : public UUserWidget
{
	GENERATED_BODY()

public:
	/**  */
	UFUNCTION(BlueprintCallable, Category = "Ability Stat")
	void SetAbilityRead(class UAbilitySystemComponent* const ASC);

public:
	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnHealthChanged(float OldValue, float NewValue);

	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnArmorChanged(float OldValue, float NewValue);

	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnAttackChanged(float OldValue, float NewValue);

	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnStrengthChanged(float OldValue, float NewValue);

	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnAgilityChanged(float OldValue, float NewValue);

	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnIntelligenceChanged(float OldValue, float NewValue);

	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnVigorChanged(float OldValue, float NewValue);

	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnKnowledgeChanged(float OldValue, float NewValue);

	/** Called when changes . */
	UFUNCTION(BlueprintImplementableEvent, Category = "Ability Stat")
	void OnSpeedChanged(float OldValue, float NewValue);

private:
	const class UCharactersAttributeSet* FindAttributeCharacter(class UAbilitySystemComponent* const InASC);
	void LoadStats(class UAbilitySystemComponent* const ASC);
	void BindFuncFromAbilityComponent(class UAbilitySystemComponent* const ASC);
	void OnHealthChangeNative(const FOnAttributeChangeData& Data);
	void OnArmorChangeNative(const FOnAttributeChangeData& Data);
	void OnAttackChangeNative(const FOnAttributeChangeData& Data);
	void OnStrengthChangeNative(const FOnAttributeChangeData& Data);
	void OnAgilityChangeNative(const FOnAttributeChangeData& Data);
	void OnIntelligenceChangeNative(const FOnAttributeChangeData& Data);
	void OnVigorChangeNative(const FOnAttributeChangeData& Data);
	void OnKnowledgeChangeNative(const FOnAttributeChangeData& Data);
	void OnSpeedChangeNative(const FOnAttributeChangeData& Data);
};
