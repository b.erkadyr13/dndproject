
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InventoryPanel.generated.h"

/**
 * UFUNCTION(BlueprintImplementableEvent, BlueprintCosmetic, Category="User Interface", meta=( Keywords="Begin Play" ))
 */
UCLASS(Abstract)
class DND_API UInventoryPanel : public UUserWidget
{
	GENERATED_BODY()

public:
	/***/
	UFUNCTION(BlueprintCallable, Category = "Inventory UI")
	void SetInventoryRead(class UInventoryComponent* const InventoryComp);

	/***/
	UFUNCTION(BlueprintCallable, Category = "Inventory UI")
	void ClearAllSlots();

	/** Find card slot. */
	UFUNCTION(BlueprintCallable, Category = "Inventory UI|Filter")
	bool FindCardBySlot(const FInventorySlot& BySlot, class UCardBase*& CardPtr) const;

	/** Find empty card. */
	UFUNCTION(BlueprintCallable, Category = "Inventory UI|Filter")
	bool FindEmptyCard(class UCardBase*& EmptyCard) const;

protected:
	/** Called when added item in inventory. */
	UFUNCTION(BlueprintImplementableEvent)
	void OnAddedItemInInventory(class UItemData* NewItem, const FInventorySlot& InSlot, class UInventoryComponent* InInventoryComp);

	/** Called when added slot in inventory. */
	UFUNCTION(BlueprintImplementableEvent)
	void OnAddedSlotInInventory(const FInventorySlot& NewSlot, class UInventoryComponent* InInventoryComp);

	/** Called when removed item from inventory. */
	UFUNCTION(BlueprintImplementableEvent)
	void OnRemovedItemInInventory(class UItemData* RemovedItem, const FInventorySlot& InSlot, class UInventoryComponent* InInventoryComp);

	/** Called when removed slot from inventory. */
	UFUNCTION(BlueprintImplementableEvent)
	void OnRemovedSlotInInventory(const FInventorySlot& RemovedSlot, class UInventoryComponent* InInventoryComp);

protected:
	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;

protected:
	/** Grid Panel. */
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	TObjectPtr<class UGridPanel> GP_Inventory;

private:
	void LoadSlots(class UInventoryComponent* const FromInventoryComp);
	void AddSlot(int32 Index, const FInventorySlot& NewSlot);
	void AddSlot(class UCardBase* InCard, const FInventorySlot& NewSlot);
	bool CreateInventoryUI(int32 Slots);
	void CreateInventoryUI(class UInventoryComponent* const InventoryComp);
	void BindFuncFromInventory(class UInventoryComponent* const InventoryComp);
	void UnBindFuncFromInventory(class UInventoryComponent* const InventoryComp);
	void SetAmountItemAt(const FInventorySlot& InSlot, int32 NewAmount);
	void SetAmountItemAt(const FInventorySlot& InSlot);

private:
	UFUNCTION()
	void OnAddedItem(class UItemData* NewItem, FInventorySlot InSlot);
	
	UFUNCTION()
	void OnAddedSlot(FInventorySlot NewSlot);

	UFUNCTION()
	void OnRemovedItem(class UItemData* RemovedItem, FInventorySlot InSlot);

	UFUNCTION()
	void OnRemovedSlot(FInventorySlot RemovedSlot);

private:
	/** Amount slots in one line. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true", ClampMin = 0), Category = "Inventory UI")
	int32 SizeRow;

	/** References inventory component whose items will be displayed in the widget. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true", ExposeOnSpawn = "true"), Category = "Inventory UI")
	TObjectPtr<class UInventoryComponent> InventoryComponent;

	/***/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Inventory UI")
	TSubclassOf<class UCardBase> CardClass;

	/** Inventory slots. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Inventory UI")
	TArray<TObjectPtr<UCardBase>> Cards;
};
