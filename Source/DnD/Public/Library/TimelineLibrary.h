
#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TimelineLibrary.generated.h"

/**
 * 
 */
UCLASS()
class DND_API UTimelineLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
    /** ����������� ������� � ���������.
     * @param Timeline,       references on timeline.
     * @param Curve,          ������ ��������������� � �������� ���������� � �������.
     * @param FuncInClass,    ����� � ������� ���� ������� ���������.
     * @param InFunctionName, ��� ������� ���������.
     * @param AutoStart,      �������� �������� ������������� ����� ����� ��������.
     * @param Rate,           �������� ���������.
     *
     * @return                ��������� true, ���� �������� ������� ��������� �������.
     */
    static bool BindFuncTimeline(struct FTimeline& Timeline, UCurveFloat* Curve, UObject* FuncInClass, const FName& InFunctionName, bool bAutoPlay = false, float Rate = 1.0f);

    /** ����������� ������� � ���������, � �������� ��� ������� ����� �������� �������� �������������.
     * @param Timeline,       ������ �� ��������.
     * @param FuncInClass,    ����� � ������� ���� ������� ���������.
     * @param InFunctionName, ��� ������� ������ ���������.
     *
     * @return                ��������� true, ���� �������� ������� ��������� �������.
     */
    static bool BindFuncTimeline(struct FTimeline& Timeline, UObject* FuncInClass, const FName& InFunctionName);
};
