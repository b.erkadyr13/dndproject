
#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "InventoryLibrary.generated.h"

/**
 * 
 */
UCLASS()
class DND_API UInventoryLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Tries to find an inventory system component on the actor, will use InventorySystemInterface or fall back to a component search. */
	UFUNCTION(BlueprintPure, Meta = (DefaultToSelf = "Actor"), Category = "Inventory Library")
	static UInventoryComponent* GetInventorySystemComponent(AActor* Actor);

	/** Searches the passed in actor for an Inventory system component, will use IInventorySystemInterface or fall back to a component search. */
	static class UInventoryComponent* GetInventorySystemComponentFromActor(const AActor* Actor, bool LookForComponent = true);

	/** Searches for items based on the specified item and removes them. 
	 * @param InActor - The actor that contains the inventory component.
	 * @param ItemData - An item that will be found in the inventory and removed from it.
	 * @param Amount - Number of items to remove.
	 * 
	 * @return Returns the number of removed items of the specified type.
	 */
	UFUNCTION(BlueprintCallable, Meta = (DefaultToSelf = "InActor"), Category = "Inventory Library")
	static int32 RemoveItemByItemFromInventory(AActor* InActor, class UItemData* ItemData, int32 Amount = 1);

	/**  */
	UFUNCTION(BlueprintCallable, Meta = (DefaultToSelf = "InActor"), Category = "Inventory Library")
	static bool RemoveItemByUniqIdFromInventory(AActor* InActor, int32 UniqueID);
	
};
