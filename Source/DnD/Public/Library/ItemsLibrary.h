
#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ItemsLibrary.generated.h"

/**
 * 
 */
UCLASS()
class DND_API UItemsLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Tries to find an ItemData on the actor, will use ItemsSystemInterface. */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "Get Item Data", DefaultToSelf = "Actor"), Category = "Items Library")
	static UItemData* GetItemDataFromActor(AActor* Actor);
	
};
