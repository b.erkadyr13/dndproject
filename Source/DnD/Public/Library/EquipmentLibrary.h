
#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EquipmentLibrary.generated.h"

/**
 * 
 */
UCLASS()
class DND_API UEquipmentLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	/** Tries to find an inventory system component on the actor, will use InventorySystemInterface or fall back to a component search. */
	UFUNCTION(BlueprintPure, Meta = (DefaultToSelf = "Actor"), Category = "Equipment Library")
	static UEquipmentComponent* GetEquipmentSystemComponent(AActor* Actor);

	/** Searches the passed in actor for an Inventory system component, will use IInventorySystemInterface or fall back to a component search. */
	static class UEquipmentComponent* GetEquipmentSystemComponentFromActor(const AActor* Actor, bool LookForComponent = true);
};
