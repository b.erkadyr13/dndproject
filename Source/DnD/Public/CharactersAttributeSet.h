// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "CharactersAttributeSet.generated.h"


#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, AActor*, OwningActor, float, NewHealth);

UCLASS()
class DND_API UCharactersAttributeSet : public UAttributeSet
{
	GENERATED_BODY()


public:
	UCharactersAttributeSet();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData HealthMax;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, HealthMax);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Health);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData ActionPoints;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, ActionPoints);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData StarterActionPoints;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, StarterActionPoints);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Armor;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Armor);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Attack;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Attack);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Strength;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Strength);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Agility;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Agility);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Intelligence;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Intelligence);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Vigor;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Vigor);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Knowledge;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Knowledge);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttributeSet")
	FGameplayAttributeData Speed;
	ATTRIBUTE_ACCESSORS(UCharactersAttributeSet, Speed);

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnHealthChanged OnHealthChanged;

	virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;
};