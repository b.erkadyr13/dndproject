// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "AbilitySet.generated.h"

/**
 * 
 */
UCLASS()
class DND_API UAbilitySet : public UDataAsset
{
	GENERATED_BODY()
	
};
