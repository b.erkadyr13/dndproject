// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "DnDGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class DND_API UDnDGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AbilityRange;

	UPROPERTY(BlueprintReadOnly)
	class ADnDCharacter* Target;

	UFUNCTION(BlueprintCallable)
	void InitializeTarget();
};
