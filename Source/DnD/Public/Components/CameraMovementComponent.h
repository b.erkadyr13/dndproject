
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CameraMovementComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DND_API UCameraMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCameraMovementComponent(const FObjectInitializer& ObjectInitializer);

public:
	/** Movement Control - W A S D. */
	UFUNCTION(BlueprintCallable, Category = "Camera Movement|Utility")
	void BasicMovementControl(float AxisValueX, float AxisValueY);

	/** Panning the camera. */
	UFUNCTION(BlueprintCallable, Category = "Camera Movement|Utility")
	void PanCamera(float RotationAmount);

	/** Set dafault Panning. */
	UFUNCTION(BlueprintCallable, Category = "Camera Movement|Utility")
	void ResetPan();

	UFUNCTION(BlueprintCallable, Category = "Camera Movement|Utility")
	void ZoomIn();

	UFUNCTION(BlueprintCallable, Category = "Camera Movement|Utility")
	void ZoomOut();

	/***/
	//UFUNCTION(BlueprintCallable, Category = "Camera Speed")
	//float SetModifierSpeed(float ModifierAmount);

	/***/
	//UFUNCTION(BlueprintCallable, Category = "Camera Speed")
	//float SetMovementSpeed(float MovementSpeedAdjusment);

	/***/
	UFUNCTION(BlueprintCallable, Category = "Camera Speed")
	bool SetCameraDisable(bool bDisableCamera);

	/***/
	//UFUNCTION(BlueprintCallable, Category = "Camera Speed")
	//bool SetZoomDisable(bool bDisableZoom);

public:
	/** Return current rotation camera. */
	FORCEINLINE FRotator GetCameraRotation() const { return GetOwner()->GetActorRotation(); }

	/** Return current location camera. */
	FORCEINLINE FVector GetCameraLocation() const { return GetOwner()->GetActorLocation(); }

	/** Return spring arm from owner component. */
	FORCEINLINE class USpringArmComponent* GetSpringArm() const;

	/** Return default movement speed the camera. */
	FORCEINLINE float GetDefaultSpeed() const { return DefaultMovementSpeed; }

	/** Return modifier movement speed. */
	FORCEINLINE float GetModifierSpeed() const { return MovementSpeedModifier; }

	/** Returns true, if the camera is not allowed to move.  */
	FORCEINLINE bool IsCameraDisable() const { return bDisableCameraMovement; }

protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	void AdjustHeight(float DeltaTime);
	void CalculateTargetHeight(float StartHeight, float& TargetHeight, int32& PreviousHeight);
	void EdgeScroll(float DeltaSecond);
	void ReceivePlayerController();

private:
	/** Is Movement Disable. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Camera Movement|Private")
	bool bDisableCameraMovement;

	/** Is Zoom Disable. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Camera Movement|Private")
	bool bDisableCameraZoom;

	/** Whether the RTS Camera should always have the same height or adjust to the world. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Camera Movement|Height Adjustment")
	bool bHeightAdjustment;

	/** Smooth camera adjustment. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", EditCondition = "bHeightAdjustment", ClampMin = 0), Category = "Camera Movement|Height Adjustment")
	float HeightAdjustmentSmoothness;

	/** The Threshold required to adjust the camera. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", EditCondition = "bHeightAdjustment", ClampMin = 0), Category = "Camera Movement|Height Adjustment")
	int Threshold;

	/** The channel used for the height adjustment */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", EditCondition = "bHeightAdjustment"), Category = "Camera Movement|Height Adjustment")
	TEnumAsByte<ECollisionChannel> TraceChannel;

	/** How Fast the Camera Moves. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Camera Movement|Speed")
	float DefaultMovementSpeed;

	/** Modifies the default movement speed when called. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Camera Movement|Speed")
	float MovementSpeedModifier;

	/** Is Edge Scroll. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Camera Movement|Edge Scroll")
	bool bEdgeScroll;

	/** Farthest Camera can get on zoom out. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", ClampMin = 0), Category = "Camera Movement|Zoom")
	float MaxZoom;

	/** Closes Camera can get on zoom in. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", ClampMin = 0), Category = "Camera Movement|Zoom")
	float MinZoom;

	/** Amount CameraArm Changes. */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", ClampMin = 0), Category = "Camera Movement|Zoom")
	float DeltaArm;

private:
	UPROPERTY(BlueprintReadOnly, Transient, meta = (AllowPrivateAccess = "true"), Category = "Camera Movement|Private")
	TObjectPtr<APlayerController> PlayerControllerPtr;
};
