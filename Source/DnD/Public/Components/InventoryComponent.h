
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"

USTRUCT(BlueprintType)
struct FInventorySlot
{
	GENERATED_BODY()

public:
	FInventorySlot()
	{}

	FInventorySlot(int32 UniqID, class UItemData* Item, int32 AmountItem)
		: UniqueID(UniqID),
		  ItemData(Item),
		  Amount(AmountItem)
	{}
public:
	/** Unique slot identifier. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 UniqueID = -1;

	/** Item that contains a slot. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class UItemData> ItemData = nullptr;

	/** Count items in slot. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0))
	int32 Amount = 0;

public:
	/* Sets default data. **/
	void ClearSlot()
	{
		UniqueID = -1;
		ItemData = nullptr;
		Amount = 0;
	}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAddSlot, FInventorySlot, NewSlot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRemoveSlot, FInventorySlot, RemovedSlot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAddItem, class UItemData*, NewItem, FInventorySlot, InSlot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnRemoveItem, class UItemData*, RemovedItem, FInventorySlot, FromSlot);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DND_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UInventoryComponent(const FObjectInitializer& ObjectInitializer);

public:
	/** Adds an item to inventory. Return true, if the item was added. */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool AddItem(class UItemData* Item);

	/** Removes one item by index. */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool RemoveItemAt(int32 Index);

	/** Removes slot by index. */
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool RemoveSlotAt(int32 Index);

public:
	 /** Finds an inventory slot for an item.
	  * @param Item,           Item Data Asset.
	  * @param ArrayID,        Index slot in array inventory.
      * @param Slot,           Found slot.
      * @param SkipFullSlot,   If true then skips filled slots.
      *
      * @return                Returns true if the slot was found.
     */
	UFUNCTION(BlueprintCallable, Category = "Inventory|Filter")
	bool FindSlotByItem(class UItemData* Item, int32& ArrayID, FInventorySlot& Slot, bool bSkipFullSlot = true) const;

	/** Finds and returns a slot by unique identifier. 	 
	  * @param UniqueID,       Unique identifier slot in inventory.
	  * @param ArrayID,        Index slot in array inventory.
      * @param Slot,           Found slot.
      *
      * @return                Returns true if the slot was found.
     */
	UFUNCTION(BlueprintCallable, Category = "Inventory|Filter")
	bool FindSlotByUniqueID(int32 UniqueID, int32& ArrayID, FInventorySlot& Slot) const;

	/** Returns true if the slot is empty. */
	UFUNCTION(BlueprintCallable, Category = "Inventory|Condition")
	bool IsFreeSlot(int32 ID) const;

	/** Returns true if there is still space in the slot.. */
	UFUNCTION(BlueprintCallable, Category = "Inventory|Condition")
	bool IsFullSlot(int32 ID) const;

	/** Returns true if there is an empty slot in inventory. */
	UFUNCTION(BlueprintCallable, Category = "Inventory|Condition")
	bool IsEmptySlot() const;

	/** Returns the total number of this type of item in the inventory. */
	UFUNCTION(BlueprintCallable, Category = "Inventory|Filter")
	int32 CountItems(class UItemData* Item) const;

public:
	/** Return list slots inventory. */
	FORCEINLINE const TArray<FInventorySlot>& GetInventory() const { return Inventory; }

	/** Return amount slots inventory. */
	FORCEINLINE int32 GetAmountSlots() const { return AmountSlots; }

public:
	/** Called when was added new item in slot inventory. */
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta = (DisplayName = "On Added New Item"))
	FOnAddItem OnAddItem;

	/** Called when was added new slot in inventory. */
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta = (DisplayName = "On Added New Slot"))
	FOnAddSlot OnAddSlot;

	/** Called when was removed item from slot inventory. */
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta = (DisplayName = "On Removed Item"))
	FOnRemoveItem OnRemovedItem;

	/** Called when was removed slot from inventory. */
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta = (DisplayName = "On Removed Slot"))
	FOnRemoveSlot OnRemovedSlot;

protected:
	virtual void BeginPlay() override;

private:
	/** Generate a unique identifier for slot. */
	int32 GenerateUniqueID(int32 MinID = 0, int32 MaxID = 9999);

private:
	/** List items a owner. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Inventory|Private")
	TArray<FInventorySlot> Inventory;

	/** Amount slots in inventory. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true", ClampMin = 0, ClampMax = 9999), Category = "Inventory")
	int32 AmountSlots = 10;
};
