
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "StatComponent.generated.h"

USTRUCT(BlueprintType)
struct FStat
{
	GENERATED_BODY()

public:
	FStat() 
	{}

	FStat(float Current, float Max)
		: CurrentAmount(Current),
		  MaximumAmount(Max)
	{}

	/** Current value stat. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (NoResetToDefault, ClampMin = 0))
	float CurrentAmount = 100.f;

	/** Max value stat. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (NoResetToDefault, ClampMin = 0))
	float MaximumAmount = 100.f;

	/** If true, stat current value is infinity. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 bStatInfinity : 1;

public:
	/** Stores previous values. */
	float OldCurrentAmount;

public:
	/** ��������� true, ���� ������� ���-�� ����������� ������� ����. */
	FORCEINLINE bool IsAbility() const { return CurrentAmount != 0.0f; }
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStatValue, FStat, NewStat);

/** ��������� ���������. */
enum class ETimelineState : uint8
{
	TS_Stop,
	TS_Recovery,
	TS_Decreases
};

UCLASS( ClassGroup=("Stats System"), meta=(BlueprintSpawnableComponent) )
class DND_API UStatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UStatComponent();

public:
	/** Called when updated value stats. */
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta = (DisplayName = "On Stat Value Update"))
	FOnStatValue OnStatValue;

protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
