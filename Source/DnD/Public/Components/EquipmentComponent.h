
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EquipmentComponent.generated.h"


UENUM(BlueprintType)
enum class EEquipment : uint8
{
	EE_None           UMETA(DisplayName = "None"),
	EE_Headwear       UMETA(DisplayName = "Headwear"),
	EE_Armour         UMETA(DisplayName = "Armour"),
	EE_Handwear       UMETA(DisplayName = "Handwear"),
	EE_Footwear       UMETA(DisplayName = "Footwear"),
	EE_Amulets        UMETA(DisplayName = "Amulets"),
	EE_Rings          UMETA(DisplayName = "Rings")
};

USTRUCT(BlueprintType)
struct FEquipManager
{
	GENERATED_BODY()

public:
	FEquipManager()
	{}

	FEquipManager(class UItemData* Item)
		: ItemData(Item)
	{}

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class UItemData> ItemData;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAddEquip, FEquipManager, NewEquip);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRemovedEquip, FEquipManager, RemovedEquip);

UCLASS( ClassGroup=("Equipment System"), meta=(BlueprintSpawnableComponent) )
class DND_API UEquipmentComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UEquipmentComponent(const FObjectInitializer& ObjectInitializer);

public:
	/** Adds new equipment. */
	UFUNCTION(BlueprintCallable, Category = "Equipment Manager")
	bool AddEquipment(EEquipment InSlot, class AItemEquipment* Equip);

	/** Removed equipment from slot. */
	UFUNCTION(BlueprintCallable, Category = "Equipment Manager")
	bool RemoveEquip(EEquipment FromSlot);

public:
	/** Get equip item from slot. Return true if equip item valid. */
	UFUNCTION(BlueprintCallable, Category = "Equipment Manager")
	bool GetEquipFromSlot(EEquipment FromSlot, class AItemEquipment*& EquipRef) const;

public:
	//============================================/DELEGATES/=====================================================================
	
	/** Called when was added new equip item. */
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta = (DisplayName = "On Added New Equipment"))
	FOnAddEquip OnAddEquip;

	/** Called when was removed equip item. */
	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable, meta = (DisplayName = "On Removed Equipment"))
	FOnRemovedEquip OnRemovedEquip;

protected:
	virtual void BeginPlay() override;

private:
	bool CheckEqualSlot(EEquipment InSlot, AItemEquipment* Equip);

private:
	/** List equipments owner. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Private")
	TMap<EEquipment, TObjectPtr<class AItemEquipment>> Equipments;
};
