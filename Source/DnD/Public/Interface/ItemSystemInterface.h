
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ItemSystemInterface.generated.h"

class UItemData;

UINTERFACE(MinimalAPI)
class UItemSystemInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class DND_API IItemSystemInterface
{
	GENERATED_BODY()

public:
	/** */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Item|Action")
	bool OnCollect(APawn* Causer);

	/**  */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Item|Action")
	bool OnDrop();

public:
	/** Return Item Data. */
	virtual UItemData* GetItemData() const = 0;
};
