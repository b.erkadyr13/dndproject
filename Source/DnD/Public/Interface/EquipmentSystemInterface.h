
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "EquipmentSystemInterface.generated.h"

class UEquipmentComponent;

UINTERFACE(MinimalAPI)
class UEquipmentSystemInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class DND_API IEquipmentSystemInterface
{
	GENERATED_BODY()

public:
	/** Returns the equipment system component to use for this actor. */
	virtual UEquipmentComponent* GetEquipmentSystemComponent() const = 0;
};

