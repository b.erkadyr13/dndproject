
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InventorySystemInterface.generated.h"

class UInventoryComponent;

UINTERFACE(MinimalAPI)
class UInventorySystemInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class DND_API IInventorySystemInterface
{
	GENERATED_BODY()

public:
	/** Returns the inventory system component to use for this actor. */
	virtual UInventoryComponent* GetInventorySystemComponent() const = 0;
};
